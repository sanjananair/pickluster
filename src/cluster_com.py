#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 10:12:37 2023

@author: luca.genz
"""


import os 
import pdb as pythondb
import numpy as np
import time
from scipy.spatial import KDTree 
import networkx 
from networkx.algorithms.components.connected import connected_components
import csv

       

def calc_interactions_per_chain(atoms_list):

    pos = []
    for i in atoms_list: 
        pos.append(i[-1])
    tree = KDTree(pos)
    pairs = list(tree.query_pairs(5))
    
    all_interactions = {}
    
    

    for interaction in pairs: 
        interact = []
        for_key = []
        for partner in interaction: 
            
            interact.append(atoms_list[partner])
            for_key.append(atoms_list[partner][0])
            
        for_key = sorted(for_key)
        key = f'{for_key}'

        
        if interact[0][0] != interact[1][0]:

            if key not in all_interactions: 
                all_interactions[key] = [interact]

            else: 
                if interact not in all_interactions[key]:
                    all_interactions[key].append(interact)
                    
    return all_interactions

def calc_interactions_per_chain_allfunc(atomsallfunc_list):

    pos = []
    for a in atomsallfunc_list: 
        position = np.array([a.coord[0],a.coord[1],a.coord[2]])
        pos.append(position)
    tree = KDTree(pos)
    pairs = list(tree.query_pairs(5))
    
    all_interactions = {}

    for interaction in pairs: 
        interact = []
        for_key = []
        for partner in interaction: 
            
            interact.append(atomsallfunc_list[partner])
            for_key.append(atomsallfunc_list[partner].residue.chain_id)
            
        for_key = sorted(for_key)
        key = f'{for_key}'

        
        if interact[0].residue.chain_id != interact[1].residue.chain_id:

            if key not in all_interactions: 
                all_interactions[key] = [interact]

            else: 
                if interact not in all_interactions[key]:
                    all_interactions[key].append(interact)
                    
    return all_interactions


def get_interaction_dics(interactions, chain1, chain2):
    
    for_key = sorted([chain1, chain2]) 
    key = f'{for_key}' 
    
    residue_interactions = {}
    atomic_residue_interactions = {}
    


    for interact in interactions[key]:
        for partner in interact: 
            res_key = f'{partner[0]}:{partner[2]}'
            
            res_int = [interact[0][:3],interact[1][:3]]
            
            if res_key not in residue_interactions:
                residue_interactions[res_key] = [res_int]
            else:
                if res_int not in residue_interactions[res_key]: 
                    residue_interactions[res_key].append(res_int)
                    
            if res_key not in atomic_residue_interactions:
                atomic_residue_interactions[res_key] = [interact]
                
            else:
                if interact not in atomic_residue_interactions[res_key]:
                    atomic_residue_interactions[res_key].append(interact)

                    
    return residue_interactions, atomic_residue_interactions
        


def to_graph(l):
    G = networkx.Graph()
    for part in l:
        G.add_nodes_from(part)
        G.add_edges_from(to_edges(part))
    return G

def to_edges(l):
    it = iter(l)
    last = next(it)

    for current in it:
        yield last, current
        last = current
        
def get_centroid(atoms_per_residue):
    
    weights = {'O': 15.999, 'C':12.011, 'N':14.007, 'S':32.06}
    
    x = 0.0
    y = 0.0
    z = 0.0
    n = 0.0
    
    for atm in atoms_per_residue:
            
        x += weights[atm[-2]] * atm[-1][0]
        y += weights[atm[-2]] * atm[-1][1]
        z += weights[atm[-2]] * atm[-1][2]
        n += weights[atm[-2]]
        
    x /= n
    y /= n
    z /= n
    return (x,y,z)
                     
def clustering_residue_based(interaction_dic, res_dic, cluster_cutoff):
    

    interface_res_clustered = {}
    
    
    for key_interface in interaction_dic:
        
        interactions = interaction_dic[key_interface]
        residues_interface ={}
        interactions_per_res = {}
        enumerated_interactions = {}
        
        
        for index, interaction in enumerate(interactions): 
            
            enumerated_interactions[index] = interaction
  
            for partner in interaction: 
                key = f'{partner[0]}'

                if key not in residues_interface:
                    residues_interface[key] = [partner[:3]]
                    
                else:
                    if partner[:3] not in residues_interface[key]:
                        residues_interface[key].append(partner[:3])
                        
                res_int_key = f'{partner[0]}:{partner[2]}'
                
                
                if res_int_key not in interactions_per_res:
                    interactions_per_res[res_int_key] = [index]
                    
                else:
                    if index not in interactions_per_res[res_int_key]:
                        interactions_per_res[res_int_key].append(index)

        clustered_chains = []  

       
        
        for key in residues_interface:
            residues = residues_interface[key]
            centroid_coords = []
            centroid_res = []
            
            
            for res in residues:
                res_key = f'{res[0]}:{res[2]}'
                centroid = get_centroid(res_dic[res_key])
                centroid_coords.append(centroid)
                centroid_res.append(res)
                
            
            centroid_tree = KDTree(centroid_coords)
           
            centroid_pairs = list(centroid_tree.query_ball_point(centroid_coords,cluster_cutoff))
            
            
            centroid_graph = to_graph(centroid_pairs)
            merged_clusters_centroids = list(connected_components(centroid_graph))
            
            
            #pythondb.set_trace()

            
            for cluster in merged_clusters_centroids:
              
                residue_clu = []
                for index in cluster:
                    
                    residue_clu.append(centroid_res[index])
    
                    
                clustered_chains.append(residue_clu)
                
        
        clustered_interactions = []
        
        
        for cluster in clustered_chains: 
            interactions = []
            
            for res in cluster: 
                key = f'{res[0]}:{res[2]}'
                for interaction in interactions_per_res[key]:
                    interactions.append(interaction)
                    
            clustered_interactions.append(interactions)

            
        cluster_graph = to_graph(clustered_interactions)
        interaction_clusters = list(connected_components(cluster_graph))
        
        final_clusters = []
        
        for cluster_index in interaction_clusters:
            
            nums = []
            

            for num in cluster_index:
                info = [enumerated_interactions[num][0][:3],enumerated_interactions[num][1][:3]]
                
                if info not in nums:
                
                    nums.append(info)
                    
            final_clusters.append(nums)

        interface_res_clustered[key_interface] = final_clusters
  
        
    return interface_res_clustered
        
def interfaces(clustered_res_dic):
    
    interface_length_per_chain = {}
    
    for key_interface in clustered_res_dic:
        
        clusters = clustered_res_dic[key_interface]
        per_cluster = []
        
        for cluster in clusters: 
            c = []
            for interaction in cluster: 
                for partner in interaction: 

                    if partner not in c: 
                        c.append(partner)
            per_cluster.append(c)

                            

        interface_length_per_chain[key_interface] = per_cluster
        
    return interface_length_per_chain

def res_interactions_all_functions(clustered_res_dic, key, model):
    
    mol_interactions = {}

    
    for index, cluster in enumerate(clustered_res_dic[key]): 
        all_int = []

        for interaction in cluster: 
            inter = []
            
            for chain in model.chains:
                for residue in chain.residues:
                    if residue is not None:
                        
                        info = [residue.chain_id, residue.name, residue.number]
                        
                        if info in interaction: 
                            if residue not in inter:
                                inter.append(residue)
                                    
            all_int.append(inter)
                                
        mol_interactions[index] = all_int
    return mol_interactions



    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    